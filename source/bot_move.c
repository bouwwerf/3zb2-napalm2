/*
	Bot movement code
	
	These are heavily modified copies from the original monster movement code.
*/

#include "g_local.h"
#include "bot.h"

#define	BOT_STEPSIZE			18
#define GROUND_TEST_EPSILON		0.25

/*
Bot_CheckBottom

Returns false if any part of the bottom of the entity is off an edge that
is not a staircase.
*/
int bot_yes, bot_no;

qboolean Bot_CheckBottom(edict_t *ent)
{
	vec3_t mins, maxs, start, stop;
	trace_t trace;
	int x, y;
	float mid, bottom;
	
	if (!ent)
	{
		return false;
	}
	
	VectorAdd(ent->s.origin, ent->mins, mins);
	VectorAdd(ent->s.origin, ent->maxs, maxs);

	// if all of the points under the corners are solid world, don't bother
	// with the tougher checks
	// the corners must be within 16 of the midpoint
	start[2] = mins[2] - 1;
	for	(x = 0; x <= 1; x++)
	{
		for	(y = 0; y <= 1; y++)
		{
			start[0] = x ? maxs[0] : mins[0];
			start[1] = y ? maxs[1] : mins[1];

			if (gi.pointcontents(start) != CONTENTS_SOLID)
			{
				goto realcheck;
			}
		}
	}

	bot_yes++;
	return true; // we got out easy

realcheck:
	bot_no++;

	//
	// check it for real...
	//
	start[2] = mins[2];
	
	// the midpoint must be within 16 of the bottom
	start[0] = stop[0] = (mins[0] + maxs[0])*0.5;
	start[1] = stop[1] = (mins[1] + maxs[1])*0.5;
	stop[2] = start[2] - 2*BOT_STEPSIZE;
	trace = gi.trace(start, vec3_origin, vec3_origin, stop, ent, MASK_PLAYERSOLID);

	if (trace.fraction == 1.0)
	{
		return false;
	}

	mid = bottom = trace.endpos[2];
	
	// the corners must be within 16 of the midpoint	
	for	(x = 0; x <= 1; x++)
	{
		for	(y = 0; y <= 1; y++)
		{
			start[0] = stop[0] = x ? maxs[0] : mins[0];
			start[1] = stop[1] = y ? maxs[1] : mins[1];
			
			trace = gi.trace(start, vec3_origin, vec3_origin, stop, ent, MASK_PLAYERSOLID);
			
			if (trace.fraction != 1.0 && trace.endpos[2] > bottom)
			{
				bottom = trace.endpos[2];
			}

			if (trace.fraction == 1.0 || mid - trace.endpos[2] > BOT_STEPSIZE)
			{
				return false;
			}
		}
	}

	bot_yes++;
	return true;
}

void Bot_CheckGround(edict_t *ent)
{
	vec3_t point, stp, v1, v2;
	trace_t trace, tracep;

	if (!ent)
	{
		return;
	}

	if (ent->client)
	{
		ent->client->zc.ground_slope = 1.0;
	}

	if (ent->velocity[2] > 100)
	{
		ent->groundentity = NULL;
		return;
	}

	// if the hull point one-quarter unit down is solid the entity is on ground
	point[0] = ent->s.origin[0];
	point[1] = ent->s.origin[1];
	point[2] = ent->s.origin[2] - GROUND_TEST_EPSILON;

	trace = gi.trace (ent->s.origin, ent->mins, ent->maxs, point, ent, MASK_BOTGROUND);

	// check steepness
	if (trace.fraction == 1.0 && (!trace.startsolid && !trace.allsolid))
	{
		ent->groundentity = NULL;
		return;
	}

	if (trace.allsolid)
	{
		VectorSet(v1,-16,-16,-24);
		VectorSet(v2,16,16,4);
		VectorCopy(ent->s.origin,stp);	
		stp[2] += 24;
		tracep = gi.trace (stp, v1, v2, point, ent, MASK_BOTGROUND);
		if (tracep.ent && !tracep.allsolid)
		{
			if (tracep.ent->classname[0] == 'f')
			{
				VectorCopy(tracep.endpos,ent->s.origin);
				ent->groundentity = tracep.ent;
				ent->groundentity_linkcount = tracep.ent->linkcount;
				gi.linkentity(ent);
				return;
			}
		}
	}

	if (!trace.allsolid)
	{
		if (ent->client)
		{
			ent->client->zc.ground_contents = trace.contents;
			ent->client->zc.ground_slope = trace.plane.normal[2];
		}
		VectorCopy(trace.endpos, ent->s.origin);
		ent->groundentity = trace.ent;
		ent->groundentity_linkcount = trace.ent->linkcount;
	}

	gi.linkentity(ent);
}

void Bot_CategorizePosition(edict_t *ent)
{
	vec3_t point;
	int cont;

	if (!ent)
	{
		return;
	}

	point[0] = ent->s.origin[0];
	point[1] = ent->s.origin[1];
	point[2] = ent->s.origin[2] + ent->mins[2] + 1;	
	cont = gi.pointcontents(point);
	if (!(cont & MASK_WATER))
	{
		ent->waterlevel = 0;
		ent->watertype = 0;
		return;
	}

	ent->watertype = cont;
	ent->waterlevel = 1;
	point[2] += 26;
	cont = gi.pointcontents(point);
	if (!(cont & MASK_WATER))
	{
		return;
	}

	ent->waterlevel = 2;
	point[2] += 22;
	cont = gi.pointcontents(point);
	if (cont & MASK_WATER)
	{
		ent->waterlevel = 3;
	}
}
